terraform {
  backend "http" {
  }

  required_providers {
    maas = {
      source  = "suchpuppet/maas" # fork on registry from https://github.com/Roblox/terraform-provider-maas the fork with most features
      version = "~> 3.1.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.1.0"
    }
    sops = {
      source  = "carlpett/sops"
      version = "~> 0.6.2"
    }
    unifi = {
      source  = "paultyng/unifi"
      version = "0.34.0"
    }
  }
}

provider "sops" {}

data "sops_file" "secrets" {
  source_file = "secrets.json"
}
